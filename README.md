# Funanga Test project

# Create login and registration forms 


## Installation

1. Clone the repository:
    ```bash
    git clone https://gitlab.com/sadiqpk33/funanga-test.git
    ```

2. Navigate to the project directory:
    ```bash
    cd funanga-test
    ```

3. Install dependencies:
    ```bash
    composer install
    ```

4. database connection:

    Rename  .env.example file in .env 
    Create mysql database and add database name and password if you have in .env file
    
5. migrate database:
    ```bash
    php artisan migrate 
    ```

## Usage

1. Open your browser and navigate to `http://127.0.0.1:8000/` to view the application.

2. we have two form Login and Registration form on same page. first we need to SignUp to insert use record in database after the you can Login
we have buttons in each for to toggle Login and Registration forms.

3. I have done all validation on back-end and front-end.

4. All data is posting through API 

5. After Login we are create token for Auth use data

6. After Login user can Logout.