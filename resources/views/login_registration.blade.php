<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"  crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <!-- Fonts -->
       

        <!-- Styles -->
      
    </head>
    <body class="antialiased">

        
        <div class="relative sm:flex sm:justify-center sm:items-center min-h-screen bg-dots-darker bg-center bg-gray-100 dark:bg-dots-lighter dark:bg-gray-900 selection:bg-red-500 selection:text-white">
            {{-- @if (Route::has('login'))
                <div class="sm:fixed sm:top-0 sm:right-0 p-6 text-right z-10">
                    @auth
                        <a href="{{ url('/home') }}" class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Home</a>
                    @else
                        <a href="{{ route('login') }}" class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Log in</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Register</a>
                        @endif
                    @endauth
                </div>
            @endif --}}
            <div class="container" id="registration-form" >
                <div class="row justify-content-md-center">
                  <div class="col col-lg-4">
                   
                   
                    <form id="sign-up" class="form-signin" mathod="post">
                        
                        <h1 class="h3 mb-3 font-weight-normal">Please Sign Up</h1>
                        <div id='error-display' style="display: none">
                           
                        </div>

                        <label for="email" class="sr-only">Email </label>
                        <input type="email"  name="email" id="email" class="form-control" placeholder="Email address" autofocus="">
                        
                        <label for="password"  class="sr-only">Password</label>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                        <div class="checkbox mb-3">
                        
                        </div>
                        <button class="btn btn-lg btn-primary btn-block" id="sign-up" type="submit">Sign Up</button>
                        <button class="btn btn-lg text-bg-secondary btn-block"  id="have-already-account" type="button">I have already Account </button>
                    </form>
                  </div>
                </div>
            </div>


            <div class="container" id="logon-form" style="display: none">
                <div class="row justify-content-md-center">
                  <div class="col col-lg-4">
                   
                    <form class="form-signin" id='sign-in' mathod="post">
                        
                        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
                        <div id='login-error-display' style="display: none"> </div>

                        <div id="logout-button" style="display: none"> <button class="btn btn-lg btn-primary btn-block" id="logout" type="button">Logout</button> </div>


                        <label for="email" class="sr-only">Email </label>
                        <input type="email"  name="email" id="email" class="form-control" placeholder="Email address" autofocus="">
                        
                        <label for="password"  class="sr-only">Password</label>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                        <div class="checkbox mb-3">
                         
                        </div>
                        <div class="checkbox mb-3">
                            <label>
                              <input type="checkbox" value="remember-me"> Remember me
                            </label>
                          </div>
                          <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                          <button class="btn btn-lg text-bg-secondary btn-block"  id="create-new-account" type="button">Create new Account </button>
                        </form>
                    
                  </div>
                </div>
            </div>

    
        </div>

        <script>
            //toggle forms
            $("#have-already-account").on('click', function(){
                $("#logon-form").show('slow');
                $("#registration-form").hide('slow');
                $('#sign-in').get(0).reset(); 
                $('#sign-up').get(0).reset(); 

            });

             //toggle forms
            $("#create-new-account").on('click', function(){
                $("#logon-form").hide('slow');
                $("#registration-form").show('slow');
                $('#sign-in').get(0).reset(); 
                $('#sign-up').get(0).reset(); 
            })
            // user registration for are post here
            $( "form#sign-up" ).on( "submit", function( event ) {
                event.preventDefault();
                var data = $('form').serialize();
                $( "#error-display" ).html('')
                $.ajax({
                    url: "<?php echo route('api-registration');?>",
                    method: 'post',
                    dataType: 'json',
                    data: $(this).serialize(),
                    success: function(response){
                        
                        errorMessages ="<div class='alert alert-info' role='alert'>you are Successfully Registor </div>";
                        $("#error-display").html(errorMessages).show( "slow");
                    },
                    error: function(jqXHR, status, error) {
                        if(jqXHR.status == 401){
                            var errorMessage = jqXHR.responseJSON.errors;
                            // Display error message to user
                            $( "#error-display" ).show( "slow", function() {
                                var errorMessages = '';
                                if( errorMessage.email !== undefined && errorMessage.email !== null && errorMessage.email !== ""){
                                    errorMessages +="<div class='alert alert-info' role='alert'>"+errorMessage.email[0]+"</div>";

                                }

                                if( errorMessage.password !== undefined && errorMessage.password !== null && errorMessage.password !== ""){
                                    errorMessages +="<div class='alert alert-info' role='alert'>"+errorMessage.password[0]+"</div>";
                                }
                                $("#error-display").html(errorMessages);
                            
                            });

                        } else if(jqXHR.status == 500){ 
                            
                            errorMessages ="<div class='alert alert-info' role='alert'>Something went wrong please try again later</div>";
                            $("#error-display").html(errorMessages);
                        }
        
                        }
                });
            });

            //user sign In here through ajax 
            $( "form#sign-in" ).on( "submit", function( event ) {
                event.preventDefault();
                var data = $('form#sign-in').serialize();
                $( "#login-error-display" ).html('');

                $.ajax({
                    url: "<?php echo route('api-login');?>",
                    method: 'post',
                    dataType: 'json',
                    data: $(this).serialize(),
                    success: function(response){

                        errorMessages ="<div class='alert alert-info' role='alert'>Hello you are Successfully logged </div>";
                        localStorage.setItem('token', response.token);
                        $("#logout-button").show( "slow");
                        $("#login-error-display").html(errorMessages).show( "slow");

                    },
                    error: function(jqXHR, status, error) {
                    
                        if(jqXHR.status == 401){
                            var error_response = jqXHR.responseJSON;
                            // Display error message to user
                            $( "#login-error-display" ).show( "slow", function() {

                                if( error_response.message !== undefined && error_response.message !== null && error_response.message !== ""){
                                    errorMessages ="<div class='alert alert-info' role='alert'>"+error_response.message+"</div>";
                                }

                                $("#login-error-display").html(errorMessages);
                            });

                        } else if(jqXHR.status == 500){ 
                            errorMessages ="<div class='alert alert-info' role='alert'>Something went wrong please try again later</div>";
                            $("#login-error-display").html(errorMessages).show( "slow");
                        }
                        }

                });
                
            });

            // user logout here through ajax 
            $('#logout').on('click', function(){
                var token = localStorage.getItem('token');
                
                $.ajax({
                    url: "<?php echo route('logout');?>",
                    method: 'post',
                    headers: {
                        'Authorization': 'Bearer ' + token // Include the token in the Authorization header
                    },
                    success: function(response){

                        errorMessages ="<div class='alert alert-info' role='alert'> you are Successfully LogOut </div>";
                        
                        $("#login-error-display").html(errorMessages).show( "slow");
                        $("#logout-button").hide( "slow");
                    },
                    error: function(jqXHR, status, error) {
                        
                            errorMessages ="<div class='alert alert-info' role='alert'>Something went wrong please try again later</div>";
                            $("#login-error-display").html(errorMessages).show( "slow");
                    }
                });
            })
          
        </script>
    </body>
    
</html>
